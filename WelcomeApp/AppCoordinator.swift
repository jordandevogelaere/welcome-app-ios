//
//  AppCoordinator.swift
//  WelcomeApp
//
//  Created by Jordan de Vogelaere on 13/12/2019.
//  Copyright © 2019 Jordan de Vogelaere. All rights reserved.
//

import Foundation
import UIKit

class AppCoordinator: NSObject, Coordinator {
    
    var rootViewController: UIViewController = {
        return UINavigationController(rootViewController: ViewController())
    }()
    
    let window: UIWindow
    
    var childCoordinators: [Coordinator] = []
    
    func start() {
        self.window.rootViewController = rootViewController
        self.window.makeKeyAndVisible()
    }
    
    @objc required init(window: UIWindow) {
        self.window = window
    }
}
