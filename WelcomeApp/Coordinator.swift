
//
//  Coordinator.swift
//  WelcomeApp
//
//  Created by Jordan de Vogelaere on 13/12/2019.
//  Copyright © 2019 Jordan de Vogelaere. All rights reserved.
//

import Foundation
import UIKit

public protocol Coordinator : class {
    
    var rootViewController: UIViewController { get }
    var childCoordinators: [Coordinator] { get set }
    
    func start()
}
